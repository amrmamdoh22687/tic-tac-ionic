import { Square } from '../square/Square';
import { SquareSymbol, Winner } from '../Utils';

import './Board.css';

export interface BoardProps {
    grid: SquareSymbol[][],
    winner: Winner | null,
    onClick: (row: number, col: number) => void,
}

export const Board = (props: BoardProps) => {
    // props attributes
    const {grid, winner, onClick} = props;

    const renderSquare = (row: number, col: number) => {
        const isWinner = (winner != null) && (
            (winner.libelle === 'R' && winner.value === row) ||
            (winner.libelle === 'C' && winner.value === col) ||
            (winner.libelle === 'TL' && row === col) ||
            (winner.libelle === 'TR' && row === grid.length-1-col)
        );

        return (
            <Square key={`grid-row-${row}-col-${col}`} 
                    boardDimension={grid.length}
                    value={grid[row][col]} 
                    winner={isWinner}
                    onClick={() => onClick(row, col)} />
        );
    }

    /* Render */

    // génération des lignes de la grille
    const board = grid.map((row, rowIndex) => {
        const squares = row.map((square, colIndex) => {
            return renderSquare(rowIndex, colIndex);
        });
        return (
            <tr key={`grid-row-${rowIndex}`} className="board-row">
                <td key={`grid-row-${rowIndex}-col-info`} className="square square-info">{rowIndex}</td>
                {squares}
            </tr>
        );
    });

    // génération des libellés des colonnes (dernière ligne)
    const colInfo = Array.from(Array(grid.length).keys()).map((index) => {
        return (
            <td key={`grid-row-info-col-${index}`} className="square square-info-l">
                {String.fromCharCode(97 + index)}
            </td>
        );
    });

    return (
        <table>
            <tbody>
                {board}
                <tr key={"grid-row-info"} className="board-row">
                    <td className="square square-info"></td>
                    {colInfo}
                </tr>
            </tbody>
        </table>
    );
}