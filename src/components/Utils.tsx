export type SquareSymbol = 'X' | 'O' | null;

export type WinSymbol = 'R' | 'C' | 'TL' | 'TR';

export interface Move {
    player: SquareSymbol,
    row: number,
    col: number
}

export interface HistoryElement {
    grid: SquareSymbol[][]
    move: Move | null
}

export interface Winner {
    libelle: WinSymbol,
    value: number | null
}

export const genMatrix = (dim: number): SquareSymbol[][] => Array(dim).fill(null).map(row => new Array(dim).fill(null));

export const getWinner = (current: HistoryElement): Winner | null => {
    if (current.move) {
        const {player, row, col} = current.move;

        // vérification ligne
        const checkRow = (currentSquare: SquareSymbol): boolean => currentSquare === player;
        if (current.grid[row].every(checkRow)) return {libelle: 'R', value: row};

        // vérification colonne
        const checkCol = (currentRow: SquareSymbol[]): boolean => currentRow[col] === player;
        if (current.grid.every(checkCol)) return {libelle: 'C', value: col};

        // vérification des diagonales
        if (row === col) {
            let i = 0;
            const checkDiagTL = (currentRow: SquareSymbol[]): boolean => currentRow[i++] === player;
            if (current.grid.every(checkDiagTL)) return {libelle:'TL', value: null};
        } else if (row === current.grid.length-1-col) {
            let i = current.grid.length-1;
            const checkDiagTR = (currentRow: SquareSymbol[]): boolean => currentRow[i--] === player;
            if (current.grid.every(checkDiagTR)) return {libelle:'TR', value: null};
        }
    }

    return null;
}
