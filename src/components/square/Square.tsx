import { SquareSymbol } from '../Utils';

import './Square.css';

export interface SquareProps {
    boardDimension: number,
    value: SquareSymbol,
    winner: boolean,
    onClick: () => void,
}

export const Square = (props: SquareProps) => {
    // props attributes
    const {boardDimension, value, winner, onClick} = props;

    const squareSize = Math.round(window.innerWidth * 0.85 / boardDimension);
    const fontSize = Math.round(squareSize * 0.7);

    return (
        <td className={`square ${winner ? "square-win" : ""}`} 
                style={{width: `${squareSize}px`, height: `${squareSize}px`, fontSize: `${fontSize}px`}} 
                onClick={onClick}>
            {value} 
        </td>
    );
}