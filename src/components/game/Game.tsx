import { IonButton, IonCheckbox, IonItem, IonItemDivider, IonLabel, IonRange } from '@ionic/react';
import { useState } from 'react';
import { Board } from '../board/Board';
import { genMatrix, getWinner, HistoryElement } from '../Utils';

import './Game.css';

export const Game = () => {
    // state attributes
    const [gamestate, setGamestate] = useState(0);
    const [order, setOrder] = useState(true);
    const [history, setHistory] = useState<HistoryElement[]>([{grid: genMatrix(3), move: null}]);
    const [step, setStep] = useState(0);
    const [player, setPlayer] = useState(true);

    /* Listeners */

    const handleChange = (value: number) => {
        setHistory([{grid: genMatrix(value), move: null}]);
    }

    const handleClick = (row: number, col: number) => {
        if (gamestate === 1) {
            const activeHistory = history.slice(0, step+1);
            const current = activeHistory[activeHistory.length-1];
            const activeGrid = current.grid.map((row) => row.slice());

            if (getWinner(current) || activeGrid[row][col]) return;

            activeGrid[row][col] = player ? 'X' : 'O';
            const move = {player: activeGrid[row][col], row: row, col: col};
            
            setHistory([...activeHistory, {grid: activeGrid, move: move}]);
            setStep(activeHistory.length);
            setPlayer(!player);
        }
    }

    const jumpTo = (step: number) => {
        const player = step % 2 === 0;
        setStep(step);
        setPlayer(player);
    }

    const startGame = () => {
        if (gamestate === 0) {
            setGamestate(1);
        } else if (gamestate === 1) { // RESET
            setStep(0);
            setPlayer(true);
        }
    }

    const stopGame = () => {
        if (gamestate === 1) {
            setGamestate(0);
            setHistory([{grid: genMatrix(3), move: null}]);
            setStep(0);
            setPlayer(true);
        }
    }

    /* Render */

    const current = history[step];
    const winner = getWinner(current);

    // génération de l'historique
    let moves = history.map((elem, currentStep) => {
        if (elem.move) {
            const desc = `#${currentStep}. ${elem.move.player}-${elem.move.row}${String.fromCharCode(97+elem.move.col)}`;
            return (
                <IonButton key={`r${elem.move.row}-c${elem.move.col}`} 
                            expand={'block'} 
                            color={currentStep === step ? "dark" : "medium"}
                            onClick={() => jumpTo(currentStep)}>
                    {desc}
                </IonButton>
            );
        }
        return null;
    });
    if (!order) moves = moves.reverse();

    // génération du status
    let status = "Tic-Tac-Toe";
    if (gamestate === 1) {
        if (winner) {
            status = `${current.move?.player} a gagné !`;
        } else if (history.length-1 === Math.pow(current.grid.length, 2)) {
            status = 'Egalité !';
        } else {
            status = `Prochain joueur: ${player ? 'X' : 'O'}`;
        }
    }

    return (
        <div className="game">
            <div className="game-board">
                <Board grid={current.grid} 
                        winner={winner} 
                        onClick={(row: number, col: number) => handleClick(row, col)} />
            </div>
            <IonItemDivider>Informations de la partie</IonItemDivider>
            <div className="game-settings">
                <span>Dimension de la grille de jeu</span>
                <IonRange pin={true} snaps={true} min={3} max={10} value={current.grid.length} 
                            disabled={gamestate === 1 ? true : false}
                            onIonChange={e => handleChange(e.detail.value as number)}>
                    <IonLabel slot="start">3</IonLabel>
                    <IonLabel slot="end">10</IonLabel>
                </IonRange>
                <IonButton expand={'block'} 
                            color={"danger"}
                            disabled={gamestate === 0 ? true : false} 
                            onClick={() => stopGame()}>
                    Stop
                </IonButton> 
                <IonButton expand={'block'} 
                            color={gamestate === 0 ? "success" : "warning"}
                            onClick={() => startGame()}>
                    {gamestate === 0 ? "Start" : "Reset"}
                </IonButton> 
            </div>
            <div className="status">{status}</div>
            <IonItem>
                <IonLabel>Ordre chronologique</IonLabel>
                <IonCheckbox checked={order} onIonChange={(e) => setOrder(e.detail.checked)} />
            </IonItem>
            <div className="history">
                {moves}
            </div>
        </div>
    );
}
